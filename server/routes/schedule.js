// Schedule
const middleware = require('../middleware.js');

// Import required models
const { ScheduleItem } = require('../models');

const imageGenerator = require('../image-generator');

module.exports = function (app) {
  app.get('/api/schedule/items/draft', middleware.isAuthenticated(), async (req, res) => {
    const scheduleItems = await ScheduleItem.find({ state: 'DRAFT' }).sort({ created: -1 });

    res.json({
      meta: { status: 200, msg: 'Schedule items - draft' },
      data: {
        items: scheduleItems,
      },
    });
  });

  app.get('/api/schedule/items/scheduled', middleware.isAuthenticated(), async (req, res) => {
    const ScheduledItems = await ScheduleItem.find({
      postAt: { $exists: true },
      state: { $in: ['PENDING', 'ERROR'] },
    }).sort({ postAt: 1 });

    const pastScheduleItems = await ScheduleItem.find({
      postAt: { $exists: true },
      state: 'POSTED',
    }).sort({ postAt: -1 });

    res.json({
      meta: { status: 200, msg: 'Schedule items - scheduled' },
      data: {
        items: {
          upcoming: ScheduledItems,
          past: pastScheduleItems,
        },
      },
    });
  });

  app.post('/api/schedule/items/schedule', middleware.isAuthenticated(), async (req, res) => {
    const date = new Date(req.body.postAt);
    const unixTime = Math.floor(date / 1000);

    const scheduleItem = await ScheduleItem.findOneAndUpdate(
      { _id: req.body._id },
      { $set: { state: 'PENDING', postAt: unixTime } },
      { new: true },
    );

    res.json({
      meta: { status: 200, msg: 'Item scheduled' },
      data: {
        item: scheduleItem,
      },
    });
  });

  app.post('/api/schedule/items/create', middleware.isAuthenticated(), async (req, res) => {
    var images = [];

    if (typeof req.body.contentType != 'undefined') {
      images.push(imageGenerator.getPrompt(req.body.contentType));
    }

    var mrkdwnText = req.body.text;

    if (mrkdwnText) {
      const slackifyMarkdown = require('slackify-markdown');
      mrkdwnText = slackifyMarkdown(req.body.text);
    }

    images = images.concat(
      req.body.files.filter((f) => {
        return f.endsWith('.jpeg') || f.endsWith('.jpg') || f.endsWith('.png');
      }),
    );

    const documents = req.body.files.filter((f) => {
      return f.endsWith('.pdf') || f.endsWith('.doc') || f.endsWith('.docx') || f.endsWith('.mp3');
    });

    for (const item in req.body.meta) {
      // Set unix timestamps
      if (
        ['questionSubmissionDeadline', 'questionDistributionDate', 'questionAwardsDate'].includes(
          item,
        )
      ) {
        const date = new Date(req.body.meta[item]);
        const unixTime = Math.floor(date / 1000);
        req.body.meta[item] = unixTime;
      }
    }

    const scheduleItem = await ScheduleItem.create({
      type: req.body.type,
      contentType: req.body.contentType,
      state: 'DRAFT',
      text: req.body.text,
      mrkdwnText: mrkdwnText,
      images: images,
      documents: documents,
      pollOptions: req.body.pollOptions,
      meta: req.body.meta,
    });

    res.json({
      meta: { status: 200, msg: 'Schedule item added' },
      data: {
        item: scheduleItem,
      },
    });
  });

  app.post('/api/schedule/items/remove', middleware.isAuthenticated(), async (req, res) => {
    console.log('req.body', req.body);

    // Get item
    const scheduleItem = await ScheduleItem.findOne({ _id: req.body._id });
    if (scheduleItem.state == 'PENDING' || scheduleItem.state == 'POSTED') {
      // If item is PENDING or POSTED mark as DRAFT
      await ScheduleItem.findOneAndUpdate(
        {
          _id: req.body._id,
        },
        { $set: { state: 'DRAFT' }, $unset: { postAt: 1 } },
      );
    } else {
      // else remove
      await ScheduleItem.deleteOne({
        _id: req.body._id,
      });
    }

    res.json({
      meta: { status: 200, msg: 'Schedule item update' },
      data: {},
    });
  });
};
