// Media
const YAML = require('yaml');
const fs = require('fs');

const { createCanvas, Image } = require('canvas');

// Import required models
const { Team, User } = require('../models');

module.exports = function (app) {
  // Used for the Global Classroom icon
  app.get('/media/app/icon', async (req, res) => {
    const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

    let identifier = 'app-icon';
    let icon = appConfig.app.icon || 'user';
    let leftColor = '#' + (appConfig.app.color || 'FF0000');
    let rightColor = '#' + (appConfig.app.color || 'FF0000');

    const iconSize = 512;
    const iconMargin = 0;
    const canvasSize = iconSize + iconMargin * 2;

    const canvas = createCanvas(canvasSize, canvasSize);
    const ctx = canvas.getContext('2d');

    var x = 0;
    var y = 0;
    var width = canvasSize;
    var height = canvasSize;
    var radius = 100;

    // Background start
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();

    ctx.clip();

    var grd = ctx.createLinearGradient(0, 0, width, 0);
    grd.addColorStop(0, leftColor);
    grd.addColorStop(1, rightColor);
    ctx.fillStyle = grd;

    drawIcon(canvas, icon, iconSize, iconMargin, iconMargin, leftColor, rightColor, true);

    const buffer = canvas.toBuffer('image/png');
    const path = `${process.env.PUBLIC_MEDIA_PATH}/icons/${identifier}.png`;
    //fs.writeFileSync(path, buffer);
    //res.sendFile(path, { root: './' });
    res.send(buffer);
  });

  // Used for the icons rendered in the ActivitySelector component in client
  app.get('/media/icon', async (req, res) => {
    let identifier = req.query.identifier || 'error';
    let icon = req.query.icon || 'exclamation';
    let leftColor = '#' + (req.query.leftColor || 'FF0000');
    let rightColor = '#' + (req.query.rightColor || 'FF0000');

    const iconSize = 512;
    const iconMargin = 0;
    const canvasSize = iconSize + iconMargin * 2;

    const canvas = createCanvas(canvasSize, canvasSize);
    const ctx = canvas.getContext('2d');

    var x = 0;
    var y = 0;
    var width = canvasSize;
    var height = canvasSize;
    var radius = 100;

    // Background start
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();

    ctx.clip();

    var grd = ctx.createLinearGradient(0, 0, width, 0);
    grd.addColorStop(0, leftColor);
    grd.addColorStop(1, rightColor);
    ctx.fillStyle = grd;
    // ctx.fillRect(0, 0, canvas.width, canvas.height);

    drawIcon(canvas, icon, iconSize, iconMargin, iconMargin, leftColor, rightColor, true);

    const buffer = canvas.toBuffer('image/png');
    const path = `${process.env.PUBLIC_MEDIA_PATH}/icons/${identifier}.png`;
    // fs.writeFileSync(path, buffer);
    // res.sendFile(path, { root: './' });
    res.send(buffer);
  });

  // Generate prompts to be shown alongside content and activites
  app.get('/media/prompt', async (req, res) => {
    let identifier = req.query.identifier || 'error';
    let icon = req.query.icon || 'exclamation';
    let title = req.query.title || 'No title';
    let subtitle = req.query.subtitle || 'No subtitle';
    let leftColor = '#' + (req.query.leftColor || 'FF0000');
    let rightColor = '#' + (req.query.rightColor || 'FF0000');

    const iconSize = 100;
    const iconMargin = 20;

    const rightPadding = 50;
    const leftPadding = 10 + iconSize + iconMargin * 2;

    const defaultTitleWidth = 960;
    const subtitleYOffset = 80;

    const minHeight = iconSize + iconMargin * 2;
    const minWidth = 640;
    var width = minWidth;
    var height = minHeight;

    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext('2d');

    var x = 0;
    var y = 0;
    var radius = 30;

    const titleTextWidth = drawTitle(canvas, title, leftPadding);
    width =
      (titleTextWidth > defaultTitleWidth ? titleTextWidth : defaultTitleWidth) +
      leftPadding +
      rightPadding;

    canvas.width = width;

    const reqHeight = drawSubtitle(canvas, subtitle, rightPadding, leftPadding, subtitleYOffset);

    height = canvas.height = reqHeight > minHeight ? reqHeight : minHeight;

    // Background start
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();

    ctx.clip();

    var grd = ctx.createLinearGradient(0, 0, width, 0);
    grd.addColorStop(0, leftColor);
    grd.addColorStop(1, rightColor);
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    drawTitle(canvas, title, leftPadding);
    drawSubtitle(canvas, subtitle, rightPadding, leftPadding, subtitleYOffset);
    drawIcon(canvas, icon, iconSize, iconMargin, iconMargin, leftColor, rightColor, false);

    const buffer = canvas.toBuffer('image/png');
    // const path = `${process.env.PUBLIC_MEDIA_PATH}/prompts/${identifier}.png`;
    // fs.writeFileSync(path, buffer);
    // res.sendFile(path, { root: './' });
    res.send(buffer);
  });

  // Generate awards with team member images, used for question activity awards
  app.get('/media/award', async (req, res) => {
    let identifier = req.query.identifier || 'error';
    let icon = req.query.icon || 'trophy';
    let title = req.query.title || 'No title';
    let subtitle = req.query.subtitle || 'No subtitle';
    let leftColor = '#' + (req.query.leftColor || 'FFD700');
    let rightColor = '#' + (req.query.rightColor || 'DAA520');
    let channelId = req.query.channelId;

    var team = undefined;
    var users = undefined;

    if (channelId) {
      team = await Team.findOne({ channelId: channelId });

      if (typeof team != 'undefined') {
        users = await User.find({ userId: team.users }).limit(32);
      }
    }

    const iconSize = 100;
    const teamImageSize = 60;
    const bottomMargin = 0;

    const iconX = 270;
    const iconY = 40;

    const rightPadding = 50;
    const leftPadding = 50;

    const titleYOffset = iconSize + iconY * 2;

    const subtitleYOffset = titleYOffset + 50;

    const minHeight = iconSize + iconY * 2;
    const minWidth = 640;
    var width = minWidth;
    var height = minHeight;

    const canvas = createCanvas(width, height);
    const ctx = canvas.getContext('2d');

    var x = 0;
    var y = 0;
    var radius = 30;

    canvas.width = width;

    let subtitleBaseline = drawSubtitle(
      canvas,
      subtitle,
      rightPadding,
      leftPadding,
      subtitleYOffset,
    );
    var teamHeight = 0;

    if (typeof users != 'undefined' && users.length > 0) {
      teamHeight = await drawTeam(canvas, users, teamImageSize, subtitleBaseline + 20);
    }

    let reqHeight = teamImageSize + bottomMargin + subtitleBaseline + teamHeight;

    height = canvas.height = reqHeight > minHeight ? reqHeight : minHeight;

    // Background start
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();

    ctx.clip();

    var grd = ctx.createLinearGradient(0, 0, width, 0);
    grd.addColorStop(0, leftColor);
    grd.addColorStop(1, rightColor);
    ctx.fillStyle = grd;
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    drawIcon(canvas, icon, iconSize, iconX, iconY, leftColor, rightColor, false);
    drawTitle(canvas, title, leftPadding, titleYOffset, true);
    drawSubtitle(canvas, subtitle, rightPadding, leftPadding, subtitleYOffset, true);
    if (typeof users != 'undefined' && users.length > 0) {
      await drawTeam(canvas, users, teamImageSize, subtitleBaseline + 20);
    }

    const buffer = canvas.toBuffer('image/png');
    // const path = `${process.env.PUBLIC_MEDIA_PATH}/awards/${identifier}.png`;
    // fs.writeFileSync(path, buffer);
    // res.sendFile(path, { root: './' });
    res.send(buffer);
  });
};

const drawIcon = function (
  canvas,
  icon,
  iconSize,
  iconX,
  iconY,
  leftColor,
  rightColor,
  invert = false,
) {
  const ctx = canvas.getContext('2d');

  var iconSVG = fs.readFileSync(`./public/fa/solid/${icon}.svg`, 'utf8');
  var image = new Image();
  var coloredSvgXml = iconSVG.replace(/<svg/g, `<svg fill="white"`);
  image.src = 'data:image/svg+xml;charset=utf-8,' + coloredSvgXml;

  const containerHeight = iconSize;
  const containerWidth = iconSize;
  const containerRadius = iconSize * 0.25;

  let size = iconSize;
  x = iconX;
  y = iconY;

  ctx.beginPath();
  ctx.moveTo(x + containerRadius, y);
  ctx.lineTo(x + containerWidth - containerRadius, y);
  ctx.quadraticCurveTo(x + containerWidth, y, x + containerWidth, y + containerRadius);
  ctx.lineTo(x + containerWidth, y + containerHeight - containerRadius);
  ctx.quadraticCurveTo(
    x + containerWidth,
    y + containerHeight,
    x + containerWidth - containerRadius,
    y + containerHeight,
  );
  ctx.lineTo(x + containerRadius, y + containerHeight);
  ctx.quadraticCurveTo(x, y + containerHeight, x, y + containerHeight - containerRadius);
  ctx.lineTo(x, y + containerRadius);
  ctx.quadraticCurveTo(x, y, x + containerRadius, y);
  ctx.closePath();
  ctx.strokeStyle = 'rgba(255, 255, 255, 0.6)';
  ctx.lineWidth = 15;
  ctx.stroke();

  var grd1 = ctx.createLinearGradient(x, 0, canvas.width, 0);
  grd1.addColorStop(0, leftColor);
  grd1.addColorStop(1, rightColor);
  ctx.fillStyle = invert ? grd1 : 'white';

  ctx.fill();

  let iconWidth = iconSize * (image.naturalWidth / image.naturalHeight);
  let iconHeight = size;

  var centerX = x + containerWidth / 2;
  var centerY = y + containerWidth / 2;

  ctx.beginPath();
  ctx.arc(centerX, centerY, (iconSize / 2) * 0.85, 0, 2 * Math.PI, false);

  var grd = ctx.createLinearGradient(x, 0, canvas.width, 0);
  grd.addColorStop(0, leftColor);
  grd.addColorStop(1, rightColor);
  ctx.fillStyle = invert ? 'rgba(255, 255, 255, 0.15)' : grd;
  ctx.fill();

  iconWidth = iconWidth * 0.5;
  iconHeight = iconHeight * 0.5;

  ctx.drawImage(
    image,
    x + (containerWidth - iconWidth) / 2,
    y + (containerHeight - iconHeight) / 2,
    iconWidth,
    iconHeight,
  );
};

const drawTitle = function (canvas, text, leftPadding, titleYOffset = 30, isCenter = false) {
  const ctx = canvas.getContext('2d');

  const titleX = isCenter ? canvas.width / 2 : leftPadding;

  ctx.font = 'bold 42px Helvetica';
  ctx.textAlign = isCenter ? 'center' : 'left';
  ctx.textBaseline = 'top';
  ctx.fillStyle = '#fff';

  ctx.fillText(text, titleX, titleYOffset);

  return ctx.measureText(text).width;
};

const drawSubtitle = function (
  canvas,
  text,
  rightPadding,
  leftPadding,
  subtitleYOffset,
  isCenter = false,
) {
  const ctx = canvas.getContext('2d');

  ctx.textAlign = isCenter ? 'center' : 'left';
  ctx.textBaseline = 'bottom';
  ctx.fillStyle = '#fff';

  const opts = {
    rect: {
      x: isCenter ? canvas.width / 2 : leftPadding,
      y: subtitleYOffset,
      width: canvas.width - rightPadding - leftPadding,
      height: canvas.height,
    },
    font: 'Helvetica',
    lineHeight: 1,
    minFontSize: 34,
    maxFontSize: 34,
  };

  const words = require('words-array')(text);

  var lines = [];

  for (var fontSize = opts.minFontSize; fontSize <= opts.maxFontSize; fontSize++) {
    var lineHeight = fontSize * opts.lineHeight;
    ctx.font = ' ' + fontSize + 'px ' + opts.font;
    var x = opts.rect.x;
    var y = opts.rect.y + fontSize; // It's the bottom line of the letters
    lines = [];
    var line = '';
    for (var word of words) {
      var linePlus = line + word + ' ';
      if (ctx.measureText(linePlus).width > opts.rect.width) {
        lines.push({ text: line, x: x, y: y });
        line = word + ' ';
        y += lineHeight;
      } else {
        line = linePlus;
      }
    }
    lines.push({ text: line, x: x, y: y });
    if (y > opts.rect.height) break;
  }
  var lastY = 0;
  for (var line of lines) {
    lastY = line.y;
    ctx.fillText(line.text.trim(), line.x, line.y);
  }
  return lastY + lineHeight;
};

async function drawImage(url, ctx) {
  let img = new Image();
  await new Promise((r) => (img.onload = r), (img.src = url));
  return img;
}
const drawTeam = async function (canvas, users, imageSize, yOffset) {
  const ctx = canvas.getContext('2d');

  let imageMargin = 15;
  let radius = 15;
  let colCount = 8;
  let rowCount = Math.ceil(users.length / colCount);

  let containerHeight = (imageSize + imageMargin) * rowCount - imageMargin;
  let containerWidth = (imageSize + imageMargin) * colCount - imageMargin;

  let currentIndex = 0;

  for (const user of users) {
    let currentCol = currentIndex % colCount;
    let currentRow = Math.ceil((currentIndex + 1) / colCount) - 1;
    let colsOnRow = users.length % colCount;
    let rowWidth =
      currentRow == rowCount - 1
        ? (imageSize + imageMargin) * colsOnRow - imageMargin
        : containerWidth;

    let image = await drawImage(user.image, ctx);

    let x = canvas.width / 2 - rowWidth / 2 + (imageSize + imageMargin) * currentCol;
    let y = yOffset + (imageSize + imageMargin) * currentRow;

    let height = imageSize;
    let width = imageSize;

    ctx.save();
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();
    ctx.clip();
    ctx.drawImage(image, x, y, width, height);
    ctx.restore();

    currentIndex++;
  }

  return containerHeight;
};
