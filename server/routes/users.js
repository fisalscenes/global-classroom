// Users
const YAML = require('yaml');
const fs = require('fs');

const middleware = require('../middleware.js');

// Import required models
const { User } = require('../models');

const updateUsersInDB = async (users) => {
  const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

  for (const user of users) {
    let userType = 'student';

    if ((appConfig.bots ?? []).includes(user.id)) {
      userType = 'bot';
    }
    if ((appConfig.facilitators ?? []).includes(user.id)) {
      userType = 'facilitator';
    }
    if ((appConfig.admins ?? []).includes(user.id)) {
      userType = 'admin';
    }

    try {
      await User.findOneAndUpdate(
        { userId: user.id },
        {
          $set: {
            userId: user.id,
            name: user.name,
            display_name: user.profile.display_name,
            real_name: user.real_name,
            image: user.profile.image_512,
            type: userType,
            deleted: user.deleted,
            is_restricted: user.is_restricted,
            is_ultra_restricted: user.is_ultra_restricted,
            raw: user,
          },
        },
        { upsert: true },
      );
      console.log('User added to DB', user.id);
    } catch (err) {
      console.error(err);
    }
  }
  console.log('Updated users in DB');
};

module.exports = function (app, slackApp) {
  // Syncs users in database with Slack members
  app.get('/api/users/sync', middleware.isAuthenticated(), async (req, res) => {
    // Get list of users from Slack
    const userList = await slackApp.client.users.list();

    // Update database with fetched users
    await updateUsersInDB(userList.members);

    res.json({
      meta: { status: 200, msg: 'Users synced' },
      data: {
        userList,
      },
    });
  });

  // Retrieve all users for a type
  app.get('/api/users/:type', middleware.isAuthenticated(), async (req, res) => {
    const users = await User.find({ type: req.params.type });

    res.json({
      meta: { status: 200, msg: 'Found users' },
      data: {
        users,
      },
    });
  });

  // Retrieve a single user by ID
  app.get('/api/user/:id', middleware.isAuthenticated(), async (req, res) => {
    const user = await User.findOne({ _id: req.params.id });

    res.json({
      meta: { status: 200, msg: 'Found user' },
      data: {
        user,
      },
    });
  });
};
