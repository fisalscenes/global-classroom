// Auth

// App config
const YAML = require('yaml');
const fs = require('fs');

module.exports = function (app, passport) {
  // Logout and destroy the user's session
  app.get('/auth/logout', function (req, res) {
    req.session.destroy();
    res.json({
      meta: { status: 200, msg: 'Logged out' },
      data: {},
    });
  });

  // Start the Slack OAuth flow
  app.get('/auth/slack', passport.authorize('Slack'));

  // Slack OAuth callback url
  app.get('/auth/slack/callback', function (req, res, next) {
    passport.authenticate('Slack', function (err, user, info) {
      if (err) {
        return next(err); // will generate a 500 error
      }

      const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

      // Check id is an admin
      if ((appConfig.admins ?? []).includes(user.id)) {
        // User is an admin
        req.login(user, function (err) {
          if (err) {
            return next(err);
          }
          return res.redirect(process.env.CLIENT_URL + '?auth=true');
        });
      } else {
        // User is NOT an admin
        return res.redirect(process.env.CLIENT_URL + '?auth=false');
      }
    })(req, res, next);
  });
};
