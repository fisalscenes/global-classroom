// Dashboard
const middleware = require('../middleware.js');

// Import required models
const { Message, User, Team, TeamQuestion } = require('../models');

module.exports = function (app) {
  // Present a basic page to show API is available
  app.get('/', async (req, res) => {
    res.render('index');
  });

  // Returns some basic stats for the deployment (rendered in Dashboard)
  app.get('/api/status', middleware.isAuthenticated(), async (req, res) => {
    res.json({
      meta: { status: 200, msg: 'Status' },
      data: {
        teamCount: await Team.countDocuments({ archived: false, deleted: false }),
        userCount: await User.countDocuments({ type: 'student' }),
        messageCount: await Message.countDocuments({}),
        teamQuestionCount: await TeamQuestion.countDocuments({}),
      },
    });
  });
};
