const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create RoleRequest Schema
const RoleRequest = new Schema(
  {
    requestingUserId: String,
    targetUserId: String,
    role: String,
    team: String,
    requestMessage: Object,
    state: {
      type: String,
      default: 'REQUESTED',
    },
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('RoleRequest', RoleRequest);
