const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create Team Schema
const Team = new Schema(
  {
    channelId: String,
    name: String,
    altNames: {
      type: Array,
      default: [],
    },
    users: {
      type: Array,
      default: [],
    },
    userCount: {
      type: Number,
      default: 0,
    },
    archived: {
      type: Boolean,
      default: false,
    },
    deleted: {
      type: Boolean,
      default: false,
    },
    received_onboarding: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('team', Team);
