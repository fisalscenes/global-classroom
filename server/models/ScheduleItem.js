const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create ScheduleItem Schema
const ScheduleItem = new Schema(
  {
    type: String, // content, activity
    contentType: String, // ice-breaker, role-assignment, question-activity, poll, general etc.
    text: String,
    mrkdwnText: String,
    images: Array,
    documents: Array,
    pollOptions: Array,
    meta: {
      type: Object,
      default: {},
    },
    raw: {
      type: Object,
      default: {},
    },
    postAt: Number, // unix epoch
    timestamps: Array,
    state: {
      type: String,
      default: 'DRAFT',
    },
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('ScheduleItem', ScheduleItem);
