const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create Poll Schema
const Poll = new Schema(
  {
    scheduleItemId: String,
    teamId: String,
    question: String,
    options: Array,
    responses: {
      type: Object,
      default: {},
    },
    timestamps: Array,
    state: {
      type: String,
      default: 'DRAFT',
    },
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('Poll', Poll);
