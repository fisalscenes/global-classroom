const mongoose = require('mongoose');

const Schema = mongoose.Schema;

// create User Schema
const User = new Schema(
  {
    userId: String,
    name: String,
    real_name: String,
    display_name: String,
    image: String,
    role: { type: String, default: 'none' },
    type: { type: String, default: 'none' },
    assignedTeam: { type: Boolean, default: false },
    deleted: { type: Boolean, default: false },
    is_restricted: { type: Boolean, default: false },
    is_ultra_restricted: { type: Boolean, default: false },
    hasEnrolled: { type: Boolean, default: false },
    onboardingSurvey: {
      organisation: String,
      cbl_experience_level: String,
      cbl_enjoyment_level: String,
      cbl_value_perception: String,
      cbl_engagement_style: String,
    },
    raw: {
      type: Object,
      default: {},
    },
  },
  {
    timestamps: { createdAt: 'created', updatedAt: 'updated' },
    usePushEach: true,
  },
);

module.exports = mongoose.model('User', User);
