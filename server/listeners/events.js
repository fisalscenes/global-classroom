// Events e.g. member joining a channel
// Respond to Slack events

const YAML = require('yaml');
const fs = require('fs');

const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));

// Import required models
const { ChannelActivity, Team, User, TeamQuestion, ScheduleItem } = require('../models');

const init = async function (app, logger, orchestration, home) {
  // A message was sent in the Slack workspace
  app.event('message', async ({ event, client, context }) => {
    // Log the message to the database
    await logger.logMessage(event);
  });

  app.event('reaction_added', async ({ event, client, context }) => {
    console.log('Reaction added', event);

    let acceptedReactions = [];
    for (const questionVoteType of i18n.questionActivity.voteTypes) {
      acceptedReactions.push(questionVoteType.emoji);
    }
    if (acceptedReactions.includes(event.reaction)) {
      try {
        const scheduleItem = await ScheduleItem.findOne({ timestamps: event.item.ts });

        // Add user vote to team question
        await TeamQuestion.findOneAndUpdate(
          { _id: scheduleItem.meta.teamQuestionId },
          { $addToSet: { votes: `${event.user}-${event.reaction}` } },
          { new: true },
        );
      } catch (error) {
        console.error(error);
      }
    }
  });

  app.event('reaction_removed', async ({ event, client, context }) => {
    console.log('Reaction removed', event);

    let acceptedReactions = [];
    for (const questionVoteType of i18n.questionActivity.voteTypes) {
      acceptedReactions.push(questionVoteType.emoji);
    }
    if (acceptedReactions.includes(event.reaction)) {
      try {
        const scheduleItem = await ScheduleItem.findOne({ timestamps: event.item.ts });

        // Remove user vote to team question
        await TeamQuestion.findOneAndUpdate(
          { _id: scheduleItem.meta.teamQuestionId },
          { $unset: { votes: `${event.user}-${event.reaction}` } },
          { new: true },
        );
      } catch (error) {
        console.error(error);
      }
    }
  });

  app.event('member_joined_channel', async ({ event, client, context }) => {
    const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

    await ChannelActivity.create(event);

    // Add user to team
    try {
      let team = await Team.findOne({ channelId: event.channel });
      let teamUsers = new Set();

      for (const userId of team.users) {
        teamUsers.add(userId);
      }

      const excludedUserIds = [
        ...(appConfig.admins ?? []),
        ...(appConfig.facilitators ?? []),
        ...(appConfig.bots ?? []),
      ];

      if (!excludedUserIds.includes(event.user)) {
        teamUsers.add(event.user);

        team = await Team.findOneAndUpdate(
          { channelId: event.channel },
          { $set: { users: [...teamUsers], userCount: teamUsers.size } },
          { upsert: false, new: true },
        );

        await home.updateHomeView(client, event.user);

        console.log('User added to team in DB');
      }
    } catch (err) {
      console.error(err);
    }
  });

  app.event('member_left_channel', async ({ event, client, context }) => {
    await ChannelActivity.create(event);

    // Remove user from team
    await Team.findOneAndUpdate({ channelId: event.channel }, { $pull: { users: event.user } });

    // Check if user is any other teams
    let usersTeams = await Team.find({ users: event.user });
    if (usersTeams.length == 0) {
      // If not in a team then update state
      await User.findOneAndUpdate({ userId: event.user }, { $set: { assignedTeam: false } });
    }
  });

  app.event('group_archive', async ({ event, client, context }) => {
    console.log('group_archive', event.channel);

    try {
      // Get the team
      let team = await Team.findOne({ channelId: event.channel });

      // Mark the team as archived and remove users
      await Team.updateMany(
        { channelId: team.channelId },
        { $set: { archived: true } },
        { $unset: { users: 1 } },
      );

      // Update team assignment and reset role
      await User.updateMany(
        { userId: team.users },
        { $set: { assignedTeam: false }, $unset: { role: 1 } },
      );
    } catch (error) {
      console.error(error); // console.error(error.data.error);
    }
  });

  app.event('group_deleted', async ({ event, client, context }) => {
    console.log('group_deleted', event.channel);

    try {
      // Get the team
      let team = await Team.findOne({ channelId: event.channel });

      if (typeof team != 'undefined') {
        // Mark the team as deleted and remove users
        await Team.updateMany(
          { channelId: team.channelId },
          { $set: { deleted: true, users: [] } },
        );

        // Update team assignment and reset role
        await User.updateMany(
          { userId: team.users },
          { $set: { assignedTeam: false }, $unset: { role: 1 } },
        );
      }
    } catch (error) {
      console.error(error);
    }
  });

  app.event('user_change', async ({ event, client, context }) => {
    console.log('user_change', event);

    const user = event.user;

    // Update in database
    try {
      await User.findOneAndUpdate(
        { userId: user.id },
        {
          $set: {
            userId: user.id,
            name: user.name,
            display_name: user.profile.display_name,
            real_name: user.real_name,
            image: user.profile.image_512,
            deleted: user.deleted,
            is_restricted: user.is_restricted,
            is_ultra_restricted: user.is_ultra_restricted,
            raw: user,
          },
        },
        { upsert: true },
      );
      console.log('User updated in DB');
    } catch (err) {
      console.error(err);
    }
  });

  app.event('team_join', async ({ event, client, context }) => {
    const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));

    // A user joined
    console.log('team_join', event);

    const user = event.user;

    var userType = 'student';

    if ((appConfig.bots ?? []).includes(user.id)) {
      userType = 'bot';
    }
    if ((appConfig.facilitators ?? []).includes(user.id)) {
      userType = 'facilitator';
    }
    if ((appConfig.admins ?? []).includes(user.id)) {
      userType = 'admin';
    }

    // Add to database
    try {
      await User.findOneAndUpdate(
        { userId: user.id },
        {
          userId: user.id,
          name: user.name,
          display_name: user.profile.display_name,
          real_name: user.real_name,
          image: user.profile.image_512,
          type: userType,
          deleted: user.deleted,
          is_restricted: user.is_restricted,
          is_ultra_restricted: user.is_ultra_restricted,
          raw: user,
        },
        { upsert: true },
      );
      console.log('User added to DB');
    } catch (err) {
      console.error(err);
    }

    // If teams have been assigned then find the user a team to join
    const teamsAssigned = (await Team.find({ archived: false, deleted: false })).length > 0;

    if (teamsAssigned) {
      console.log(
        'Teams have already been assigned, adding new user to a random team',
        user.real_name,
      );
      orchestration.assignTeams(client);
    } else {
      console.log('Teams have not yet been assigned');
    }
  });
};

module.exports = {
  init,
};
