require('dotenv').config();

const express = require('express');
const cors = require('cors');
const multer = require('multer');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo');
const bodyParser = require('body-parser');
const fs = require('fs');

const { App, ExpressReceiver } = require('@slack/bolt');

const receiver = new ExpressReceiver({ signingSecret: process.env.SLACK_SIGNING_SECRET });

const { SLACK_CLIENT_ID, SLACK_CLIENT_SECRET } = process.env,
  SlackStrategy = require('passport-slack-oauth2').Strategy,
  passport = require('passport');

receiver.app.use(cookieParser()); // Auth cookies

// Import required models
const AdminUser = require('./models/AdminUser.js');

let app = new App({
  token: process.env.SLACK_BOT_TOKEN,
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  appToken: process.env.SLACK_APP_TOKEN,
  receiver: receiver,
});

// Configure Multer for file upload
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, require('path').join(__dirname, process.env.PUBLIC_MEDIA_PATH));
  },
  filename: function (req, file, cb) {
    let extension = '';

    switch (file.mimetype) {
      case 'image/jpeg':
        extension = '.jpg';
        break;

      case 'image/png':
        extension = '.png';
        break;

      case 'application/pdf':
        extension = '.pdf';
        break;

      case 'audio/mpeg':
      case 'audio/mp3':
        extension = '.mp3';
        break;

      default:
        extension = '';
        break;
    }
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1e9);
    cb(null, uniqueSuffix + extension);
  },
});

// Ensure media directories are created (may have been ignored by Git due to being empty)
if (!fs.existsSync(`${process.env.PUBLIC_MEDIA_PATH}/icons`))
{
  console.log('creating icons dir')
  fs.mkdirSync(`${process.env.PUBLIC_MEDIA_PATH}/icons`, { recursive: true });
}
if (!fs.existsSync(`${process.env.PUBLIC_MEDIA_PATH}/prompts`))
{
  console.log('creating prompts dir')
  fs.mkdirSync(`${process.env.PUBLIC_MEDIA_PATH}/prompts`, { recursive: true });
}
if (!fs.existsSync(`${process.env.PUBLIC_MEDIA_PATH}/awards`))
{
  console.log('creating awards dir')
  fs.mkdirSync(`${process.env.PUBLIC_MEDIA_PATH}/awards`, { recursive: true });
}

// Configure image upload
const upload = multer({ storage: storage });

// Configure CORS
receiver.app.use(
  cors({
    credentials: true,
    optionsSuccessStatus: 200,
    // origin:['*']
    origin: [process.env.CLIENT_URL], // '*' // process.env.API_URL
    methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'],
    headers: [
      'X-CSRF-Token',
      'X-Requested-With',
      'Accept',
      'Accept-Version',
      'Content-Length',
      'Content-MD5',
      'Content-Type',
      'Date',
      'X-Api-Version',
      'Cache-Control',
      'X-Forwarded-For',
    ],
  }),
);

// Setup pug view engine
receiver.app.set('view engine', 'pug');
receiver.app.set('views', './views');

// Configure body parser
receiver.app.use(bodyParser.json());
receiver.app.use(bodyParser.urlencoded({ extended: true }));



// Database setup
const configDB = require('./config/database.js');
mongoose.connect(configDB.url);

let options = {
  mongoUrl: process.env.DB_URL,
  collectionName: 'sessions',
  touchAfter: 24 * 3600, // = 1 day (retouch)
  ttl: 14 * 24 * 60 * 60, // = 14 days (expires)
};

// Configure sessions
let sessionConfig = {
  store: MongoStore.create(options),
  secret: process.env.PASSPORT_SESSION_SECRET || 'super-secret',
  name: 'global-classroom-session',
  resave: false,
  saveUninitialized: true,
  cookie: {
    secure: true,
    sameSite: 'None',
  },
};

receiver.app.set('trust proxy', 1);

const sessionMiddleware = session(sessionConfig);

// Setup Slack passport strategy
passport.use(
  new SlackStrategy(
    {
      clientID: SLACK_CLIENT_ID,
      clientSecret: SLACK_CLIENT_SECRET,
      skipUserProfile: false,
      scope: ['identity.basic', 'identity.email', 'identity.avatar', 'identity.team'],
    },
    async (accessToken, refreshToken, profile, done) => {
      // Persist user data in database
      await AdminUser.findOneAndUpdate(
        { slackId: profile.id },
        { slackId: profile.id ?? 'UNKNOWN', raw: profile },
        { new: true, upsert: true },
      );

      done(null, profile);
    },
  ),
);

// Serialize user
passport.serializeUser((user, done) => {
  done(null, user.id);
});

// Deserialize user
passport.deserializeUser((id, done) => {
  AdminUser.findOne({ slackId: id }, (err, user) => {
    if (err) {
      done(null, false, { error: err });
    } else {
      done(null, user);
    }
  });
});

receiver.app.use(sessionMiddleware);
receiver.app.use(passport.initialize());
receiver.app.use(passport.session()); // Persist sessions

// Logging
const logger = require('./logger');

// Views
const home = require('./surfaces/home');
home.init(app);

// Orchestration
const orchestration = require('./orchestration');

// Modals
const questionActivity = require('./modals/question-activity');
const onboarding = require('./modals/onboarding');
const changeTeamName = require('./modals/change-team-name');
const changeRole = require('./modals/change-role');
questionActivity.init(app, orchestration);
changeTeamName.init(app, orchestration);
changeRole.init(app, orchestration);
onboarding.init(app, home);

// Listeners
const actions = require('./listeners/actions');
const events = require('./listeners/events');
actions.init(app, onboarding, orchestration, questionActivity);
events.init(app, logger, orchestration, home);

// Routes
require('./routes/auth.js')(receiver.app, passport);
require('./routes/dashboard.js')(receiver.app);
require('./routes/media.js')(receiver.app);
require('./routes/schedule.js')(receiver.app);
require('./routes/teams.js')(receiver.app, app, orchestration, home);
require('./routes/upload.js')(receiver.app, upload);
require('./routes/users.js')(receiver.app, app);

// Setup cron
require('./cron.js')(app, orchestration);

// Serve static files
receiver.app.use(express.static('public'));

(async () => {
  // Start express app
  await app.start({ port: 3000 });

  console.log('Global Classroom - ⚡️ Bolt app is running! - port 3000');

  console.log(
    `Edit manifest? - https://app.slack.com/app-settings/${process.env.SLACK_WORKSPACE_ID}/${process.env.SLACK_APP_ID}/app-manifest`,
  );
})();
