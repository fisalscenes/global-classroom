// Middleware for Express.js

module.exports = {
  // Enure the user is authenticated
  isAuthenticated() {
    return function (req, res, next) {
      if (req.isAuthenticated()) {
        next();
      } else {
        return res.json({
          meta: { status: '401', notice: 'Not authenticated' },
        });
      }
    };
  },
};
