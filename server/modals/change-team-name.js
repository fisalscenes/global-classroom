// Change team name

const YAML = require('yaml');
const fs = require('fs');

const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));

const { sentenceCase, paramCase } = require('change-case');

// Import required models
const { User, Team, TeamNameRequest } = require('../models');

const blockHelper = require('../block-helper');

const init = function (app, orchestration) {
  // Handle a view_submission request
  app.view('change_team_name_modal', async ({ ack, body, view, client, logger }) => {
    // Acknowledge the view_submission request
    await ack();

    try {
      const newName = paramCase(view.state.values.name.name_input.value); // Format name input
      const channelId = view.state.values.channel.channel_input.selected_option.value;

      const userId = body['user']['id'];
      const requestingUser = await User.findOne({ userId: userId });
      const team = await Team.findOne({ channelId: channelId });

      // Message user who requested the change
      await orchestration.postMessage(client, {
        channel: userId,
        text: (() => {
          return i18n.teamName.changeModal.requestReceived
            .replaceAll(/__CURRENT_NAME__/g, sentenceCase(team.name))
            .replaceAll(/__NEW_NAME__/g, sentenceCase(newName));
        })(),
      });

      // Message team
      await orchestration.postMessage(client,{
        channel: channelId,
        text: (() => {
          return i18n.teamName.changeModal.requestReceivedTeam
            .replaceAll(/__REQUESTING_USER_REAL_NAME__/g, `<@${requestingUser.userId}>`)
            .replaceAll(/__TEAM_NAME__/g, sentenceCase(team.name))
            .replaceAll(/__CURRENT_NAME__/g, sentenceCase(newName));
        })(),
      });

      // Message admin with request
      // Get first admin
      const appConfig = YAML.parse(fs.readFileSync('./config/app.yaml', 'utf8'));
      const adminId = appConfig.admins[0];

      const requestMessage = await orchestration.postMessage(client, {
        channel: adminId,
        text: i18n.teamName.changeModal.requestReceivedAdmin.plainText,
        blocks: [
          blockHelper.markdownBlock(
            i18n.teamName.changeModal.requestReceivedAdmin.markdown
              .replaceAll(/__REQUESTING_USER_REAL_NAME__/g, `<@${requestingUser.userId}>`)
              .replaceAll(/__CURRENT_NAME__/g, sentenceCase(team.name))
              .replaceAll(/__NEW_NAME__/g, sentenceCase(newName)),
          ),
          {
            type: 'actions',
            elements: [
              {
                type: 'button',
                style: 'primary',
                text: {
                  type: 'plain_text',
                  text: i18n.general.acceptRequest,
                  emoji: true,
                },
                value: 'accepted',
                action_id: 'button_team_name_change_accepted',
                confirm: {
                  title: {
                    type: 'plain_text',
                    text: i18n.general.confirmation,
                  },
                  text: {
                    type: 'mrkdwn',
                    text: (() => {
                      return i18n.teamName.changeModal.requestAcceptWarning.replaceAll(
                        /__NEW_NAME__/g,
                        newName,
                      );
                    })(),
                  },
                  confirm: {
                    type: 'plain_text',
                    text: i18n.general.acceptRequest,
                  },
                  deny: {
                    type: 'plain_text',
                    text: i18n.general.cancel,
                  },
                },
              },
              {
                type: 'button',
                style: 'danger',
                text: {
                  type: 'plain_text',
                  text: i18n.general.rejectRequest,
                  emoji: false,
                },
                value: 'rejected',
                action_id: 'button_team_name_change_rejected',
                confirm: {
                  title: {
                    type: 'plain_text',
                    text: i18n.general.confirmation,
                  },
                  text: {
                    type: 'mrkdwn',
                    text: (() => {
                      return i18n.teamName.changeModal.requestRejectWarning.replaceAll(
                        /__CURRENT_NAME__/g,
                        sentenceCase(team.name),
                      );
                    })(),
                  },
                  confirm: {
                    type: 'plain_text',
                    text: i18n.general.rejectRequest,
                  },
                  deny: {
                    type: 'plain_text',
                    text: i18n.general.cancel,
                  },
                },
              },
            ],
          },
        ],
      });

      // Add request to DB
      await TeamNameRequest.create(
        {
          requestingUserId: requestingUser.userId,
          channelId: team.channelId,
          newName: newName,
          requestMessage: requestMessage,
        },
        { new: true },
      );
    } catch (error) {
      console.error(error);
    }
  });

  // Command - /change-team-name
  app.command('/change-team-name', async ({ command, ack, say, respond, client, body }) => {
    // Acknowledge command request
    await ack();

    await showModal(client, body);
  });
};

const showModal = async function (client, body) {
  const userId = body['user_id'];
  const teams = await Team.find({ users: userId });

  let initial_option = undefined;

  const options = teams.map((t) => {
    const option = {
      text: {
        type: 'plain_text',
        emoji: false,
        text: sentenceCase(t.name),
      },
      value: t.channelId,
    };

    if (t.channelId === body['channel_id']) {
      initial_option = option;
    }
    return option;
  });

  try {
    // Call views.open with the built-in client
    const result = await client.views.open({
      // Pass a valid trigger_id within 3 seconds of receiving it
      trigger_id: body.trigger_id,
      // View payload
      view: {
        type: 'modal',
        // View identifier
        callback_id: 'change_team_name_modal',
        title: {
          type: 'plain_text',
          text: i18n.teamName.changeModal.title,
        },
        blocks: [
          blockHelper.markdownBlock(i18n.teamName.changeModal.body),
          {
            type: 'input',
            block_id: 'channel',
            label: {
              type: 'plain_text',
              text: i18n.teamName.changeModal.prompt,
            },
            element: {
              action_id: 'channel_input',
              type: 'static_select',
              placeholder: {
                type: 'plain_text',
                text: i18n.teamName.changeModal.selectTeamPlaceholder,
              },
              initial_option: initial_option,
              options: options,
            },
          },
          {
            type: 'input',
            block_id: 'name',
            label: {
              type: 'plain_text',
              text: i18n.teamName.changeModal.inputPrompt,
            },
            element: {
              type: 'plain_text_input',
              action_id: 'name_input',
              multiline: false,
            },
          },
        ],
        submit: {
          type: 'plain_text',
          text: i18n.general.requestChange,
        },
        private_metadata: JSON.stringify({
          channel_id: body.channel_id,
          channel_name: body.channel_name,
        }),
      },
    });
  } catch (error) {
    console.error(error);
  }
};

module.exports = {
  init,
};
