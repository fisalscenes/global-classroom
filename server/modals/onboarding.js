// Onboarding

const YAML = require('yaml');
const fs = require('fs');
const i18n = YAML.parse(fs.readFileSync('./i18n.yaml', 'utf8'));

const User = require('../models/User');

// What is your experience level of online CBL so far? (loads, none, etc)
// How much do you typically enjoy online CBL? (lots, not at all etc)
// How valuable do you think online CBL is to your learning? (...)
// How do you normally like to contribute to group work? (lead, sit back, etc)

const onboardingFormBlocks = [
  {
    type: 'input',
    block_id: 'organisation',
    label: {
      type: 'plain_text',
      text: 'Which university are you from?',
    },
    element: {
      action_id: 'organisation_input',
      type: 'static_select',
      placeholder: {
        type: 'plain_text',
        emoji: false,
        text: 'Choose an option',
      },
      options: [
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'Malaysia',
          },
          value: 'malaysia',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'Monash',
          },
          value: 'monash',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'Warwick',
          },
          value: 'warwick',
        },
      ],
    },
  },
  {
    type: 'input',
    block_id: 'cbl_experience_level',
    label: {
      type: 'plain_text',
      text: 'What is your experience level of online CBL so far?',
    },
    element: {
      action_id: 'cbl_experience_level_input',
      type: 'static_select',
      placeholder: {
        type: 'plain_text',
        emoji: false,
        text: 'Choose an option',
      },
      options: [
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'None',
          },
          value: 'none',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'Little',
          },
          value: 'little',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'Some',
          },
          value: 'some',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'Lots',
          },
          value: 'lots',
        },
      ],
    },
  },
  {
    type: 'input',
    block_id: 'cbl_enjoyment_level',
    label: {
      type: 'plain_text',
      text: 'How much do you typically enjoy online CBL?',
    },
    element: {
      action_id: 'cbl_enjoyment_level_input',
      type: 'static_select',
      placeholder: {
        type: 'plain_text',
        emoji: false,
        text: 'Choose an option',
      },
      options: [
        {
          text: {
            type: 'plain_text',
            emoji: true,
            text: ':unamused: Not at all',
          },
          value: 'not-at-all',
        },
        {
          text: {
            type: 'plain_text',
            emoji: true,
            text: ':slightly_smiling_face: A little',
          },
          value: 'a-little',
        },
        {
          text: {
            type: 'plain_text',
            emoji: true,
            text: ':smile: Lots',
          },
          value: 'lots',
        },
        {
          text: {
            type: 'plain_text',
            emoji: true,
            text: ':grinning: Too much',
          },
          value: 'too-much',
        },
      ],
    },
  },
  {
    type: 'input',
    block_id: 'cbl_value_perception',
    label: {
      type: 'plain_text',
      text: 'How important do you think online CBL is to your learning?',
    },
    element: {
      action_id: 'cbl_value_perception_input',
      type: 'static_select',
      placeholder: {
        type: 'plain_text',
        emoji: false,
        text: 'Choose an option',
      },
      options: [
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'Not important',
          },
          value: 'not-important',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'A bit important',
          },
          value: 'a-bit-important',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'Somewhat important',
          },
          value: 'somewhat-important',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'Very important',
          },
          value: 'very-important',
        },
      ],
    },
  },
  {
    type: 'input',
    block_id: 'cbl_engagement_style',
    label: {
      type: 'plain_text',
      text: 'How do you normally like to contribute to group work?',
    },
    element: {
      action_id: 'cbl_engagement_style_input',
      type: 'static_select',
      placeholder: {
        type: 'plain_text',
        emoji: false,
        text: 'Choose an option',
      },
      options: [
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'I like to lead!',
          },
          value: 'i-like-to-lead',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'I like to be involved with a little bit of everything',
          },
          value: 'i-like-to-be-involved-with-a-little-bit-of-everything',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'I like a very specific job',
          },
          value: 'i-like-a-very-specific-job',
        },
        {
          text: {
            type: 'plain_text',
            emoji: false,
            text: 'I like to sit back',
          },
          value: 'i-like-to-sit-back',
        },
      ],
    },
  },
];

const init = function (app, home) {
  // Handle a view_submission request
  app.view('enrol_modal', async ({ ack, body, view, client, logger }) => {
    // Acknowledge the view_submission request
    await ack();

    // Message the user
    try {
      const userId = body['user']['id'];
      const userName = body['user']['real_name'];

      try {
        await User.findOneAndUpdate(
          { userId: userId },
          {
            $set: {
              hasEnrolled: true,
              onboardingSurvey: {
                organisation:
                  view.state.values.organisation.organisation_input.selected_option.value,
                cbl_experience_level:
                  view.state.values.cbl_experience_level.cbl_experience_level_input.selected_option
                    .value,
                cbl_enjoyment_level:
                  view.state.values.cbl_enjoyment_level.cbl_enjoyment_level_input.selected_option
                    .value,
                cbl_value_perception:
                  view.state.values.cbl_value_perception.cbl_value_perception_input.selected_option
                    .value,
                cbl_engagement_style:
                  view.state.values.cbl_engagement_style.cbl_engagement_style_input.selected_option
                    .value,
              },
            },
          },
        );
      } catch (error) {
        console.error(error);
      }

      await orchestration.postMessage(client, {
        channel: userId,
        text: i18n.enrol.confirmationMessage.replaceAll(/__USER_NAME__/g, `<@${userId}>`),
      });

      await home.updateHomeView(client, userId);
    } catch (error) {
      console.error(error);
    }
  });
};

const showEnrolModal = async function (client, body) {
  try {
    // Call views.open with the built-in client
    const result = await client.views.open({
      // Pass a valid trigger_id within 3 seconds of receiving it
      trigger_id: body.trigger_id,
      // View payload
      view: {
        type: 'modal',
        // View identifier
        callback_id: 'enrol_modal',
        title: {
          type: 'plain_text',
          text: i18n.enrol.header,
        },
        blocks: onboardingFormBlocks,
        submit: {
          type: 'plain_text',
          text: i18n.enrol.modalSubmit,
        },
      },
    });
  } catch (error) {
    console.error(error);
  }
};

module.exports = {
  init,
  showEnrolModal,
};
