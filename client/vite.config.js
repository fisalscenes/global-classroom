import { fileURLToPath, URL } from "url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

import ViteYaml from "@modyfi/vite-plugin-yaml";
import vitePluginRaw from "vite-plugin-raw";

const path = require("path");

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    ViteYaml(),
    vitePluginRaw({
      match: /\.md$/,
    }),
  ],
  server: { https: true, port: 3002 },
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
});
