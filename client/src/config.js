export const API = import.meta.env.VITE_APP_API;
export const MEDIA_BASE_URL = import.meta.env.VITE_MEDIA_BASE_URL;
export const SLACK_AUTH_URL = import.meta.env.VITE_SLACK_AUTH_URL;
export const SLACK_WORKSPACE_ID = import.meta.env.VITE_SLACK_WORKSPACE_ID;
