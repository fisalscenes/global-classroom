import status from "@/api/status";
import schedule from "@/api/schedule";
import teams from "@/api/teams";
import users from "@/api/users";

export default {
  status,
  schedule,
  teams,
  users,
};
