import * as config from "@/config.js";
import axios from "axios";

export default {
  getTeams(cb, errorCb) {
    axios.get(`${config.API}/api/teams`, { withCredentials: true }).then(
      (response) => {
        cb(response.data.data.teams);
      },
      (response) => {
        errorCb(response);
      }
    );
  },
  getTeam(id, cb, errorCb) {
    axios.get(`${config.API}/api/team/${id}`, { withCredentials: true }).then(
      (response) => {
        cb(response.data.data.team);
      },
      (response) => {
        errorCb(response);
      }
    );
  },
  getTeamMembers(id, cb, errorCb) {
    axios
      .get(`${config.API}/api/team/${id}/members`, { withCredentials: true })
      .then(
        (response) => {
          cb(response.data.data.members);
        },
        (response) => {
          errorCb(response);
        }
      );
  },
  assignTeams(cb, errorCb) {
    axios
      .post(`${config.API}/api/teams/assign`, {}, { withCredentials: true })
      .then(
        (response) => {
          cb(response.data.data);
        },
        (response) => {
          errorCb(response);
        }
      );
  },
  unassignTeams(cb, errorCb) {
    axios
      .post(`${config.API}/api/teams/unassign`, {}, { withCredentials: true })
      .then(
        (response) => {
          cb(response.data.data);
        },
        (response) => {
          errorCb(response);
        }
      );
  },
  archiveTeamChannels(cb, errorCb) {
    axios
      .post(`${config.API}/api/teams/archive`, {}, { withCredentials: true })
      .then(
        (response) => {
          cb(response.data.data);
        },
        (response) => {
          errorCb(response);
        }
      );
  },
  archiveTeamChannel(teamId, cb, errorCb) {
    axios
      .post(
        `${config.API}/api/teams/archive/${teamId}`,
        {},
        { withCredentials: true }
      )
      .then(
        (response) => {
          cb(response.data.data);
        },
        (response) => {
          errorCb(response);
        }
      );
  },
  deleteTeamChannels(cb, errorCb) {
    axios
      .post(`${config.API}/api/teams/delete`, {}, { withCredentials: true })
      .then(
        (response) => {
          cb(response.data.data);
        },
        (response) => {
          errorCb(response);
        }
      );
  },
  resetAll(cb, errorCb) {
    axios
      .post(`${config.API}/api/reset/all`, {}, { withCredentials: true })
      .then(
        (response) => {
          cb(response.data);
        },
        (response) => {
          errorCb(response);
        }
      );
  },
};
