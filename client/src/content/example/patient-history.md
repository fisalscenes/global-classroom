**Presenting complaint**
Dizziness and palpitations

**History of presenting complaint**
Alvita has noticed dizziness every now and then over the last couple of months, she is not sure exactly when it started, the dizziness is not there all the time and it happens when her heart is going fast, recently she has felt so dizzy she needed to lie down and this stopped her working. This morning it started again just as she was about to go and have her lunch, she felt terrible so rang the surgery.

She has not had any chest pain but feels a little short of breath when it happens. She has not lost consciousness. The episodes last from a few minutes to 30 minutes. Nothing she has tried makes it better (so far she has tried lying down, holding her breath, breathing quietly, taking paracetamol none of these have helped at all!). She feels slightly sick.

**Past medical history**
Last year she was diagnosed with hypertension and started on treatment after 24 hour BP monitoring. She is not overweight (last BMI 24.5 Kg.m2)

**Current medication & allergies**
She takes Amlodipine 5mg once a day for her blood pressure. No known drug allergies.

**Family history**
Her grandfather died of a heart attack age 60
Her brother has high blood pressure
There is no other known family history of heart problems

**Personal & social history**
She runs her own business, a florist shop – there have been recent issues with the lease and a large rent increase and decreased income from Covid
She lives at home with her husband, who lost his job 2 years ago
She has never smoked
She drinks 1-2 small glasses of wine about 4 times a week
She has been working very late recently and is tired and stressed
