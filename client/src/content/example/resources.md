**Palpitations**
[https://cks.nice.org.uk/topics/palpitations/](https://cks.nice.org.uk/topics/palpitations/)
[https://patient.info/doctor/palpitations-pro](https://patient.info/doctor/palpitations-pro)

**Dizziness**
[https://patient.info/doctor/dizziness-giddiness-and-feeling-faint](https://patient.info/doctor/dizziness-giddiness-and-feeling-faint)

**Hypertension**
[http://bhsoc.org](http://bhsoc.org/)
[https://www.bhf.org.uk/heart-health/risk-factors/high-blood-pressure](https://www.bhf.org.uk/heart-health/risk-factors/high-blood-pressure)

**Cardiovascular Examination**
WMS Video from Clinical Skills [ https://youtu.be/jyuekxnXuy4](https://youtu.be/jyuekxnXuy4)
Geeky Medics [ https://geekymedics.com/cardiovascular-examination-2/](https://geekymedics.com/cardiovascular-examination-2/)

**ECGs**
[http://geekymedics.com/understanding-an-ecg](http://geekymedics.com/understanding-an-ecg)
[https://www.oxfordmedicaleducation.com/ecgs/ecg-interpretation/](https://www.oxfordmedicaleducation.com/ecgs/ecg-interpretation/)

**Arrhythmias**
[https://www.bhf.org.uk/heart-health/conditions/abnormal-heart-rhythms](https://www.bhf.org.uk/heart-health/conditions/abnormal-heart-rhythms)
[https://patient.info/health/abnormal-heart-rhythms-arrhythmias](https://patient.info/health/abnormal-heart-rhythms-arrhythmias)
[http://www.heartrhythmalliance.org/aa/uk/for-patients](http://www.heartrhythmalliance.org/aa/uk/for-patients)

**Atrial Fibrillation**
[http://www.heartrhythmalliance.org/aa/uk/atrial-fibrillation](http://www.heartrhythmalliance.org/aa/uk/atrial-fibrillation)
[http://www.heartrhythmalliance.org/afa/uk/atrial-fibrillation](http://www.heartrhythmalliance.org/afa/uk/atrial-fibrillation)
[https://lifeinthefastlane.com/ecg-library/atrial-fibrillation/](https://lifeinthefastlane.com/ecg-library/atrial-fibrillation/)
[http://www.nottingham.ac.uk/nursing/practice/resources/cardiology/fibrillation/what_af_looks_like.php](http://www.nottingham.ac.uk/nursing/practice/resources/cardiology/fibrillation/what_af_looks_like.php)
Toolkit launched this year providing methodologies, resources and support for clinicians working to reduce AF related strokes:
[https://aftoolkit.co.uk/](https://aftoolkit.co.uk/) -
[https://www.nice.org.uk/guidance/cg180/chapter/1-Recommendations#management-for-people-presenting-acutely-with-atrial-fibrillation](https://www.nice.org.uk/guidance/cg180/chapter/1-Recommendations)

**Heart disease and genetics** [https://www.bhf.org.uk/heart-health/conditions/inherited-heart-conditions](https://www.bhf.org.uk/heart-health/conditions/inherited-heart-conditions)

**Management of AF**
Full NICE guidelines: [ https://www.nice.org.uk/guidance/cg180](https://www.nice.org.uk/guidance/cg180)
NICE guidelines summarised: [ https://cks.nice.org.uk/topics/atrial-fibrillation/](https://cks.nice.org.uk/topics/atrial-fibrillation/)
[https://www.nice.org.uk/guidance/cg180/chapter/1-Recommendations#management-for-people-presenting-acutely-with-atrial-fibrillation](https://www.nice.org.uk/guidance/cg180/chapter/1-Recommendations)
Statement on use of NOACs: [ https://www.nice.org.uk/guidance/cg180/resources/nic-consensus-statement-on-the-use-of-noacs-pdf-243733501](https://www.nice.org.uk/guidance/cg180/resources/nic-consensus-statement-on-the-use-of-noacs-pdf-243733501)
Toolkit launched this year providing methodologies, resources and support for clinicians working to reduce AF related strokes:
[https://aftoolkit.co.uk/](https://aftoolkit.co.uk/) -
Patient decision aid about whether to take anticoagulation (this is a really good resource!)
[https://www.nice.org.uk/guidance/cg180/resources/patient-decision-aid-pdf-243734797](https://www.nice.org.uk/guidance/cg180/resources/patient-decision-aid-pdf-243734797)
Warfarin book: [ https://www.medicines.org.uk/emc/rmm/1081/Document](https://www.medicines.org.uk/emc/rmm/1081/Document)

**Calculators:**
[https://www.mdcalc.com/cha2ds2-vasc-score-atrial-fibrillation-stroke-risk](https://www.mdcalc.com/cha2ds2-vasc-score-atrial-fibrillation-stroke-risk)
[https://clincalc.com/Cardiology/Stroke/CHADSVASC.aspx](https://clincalc.com/Cardiology/Stroke/CHADSVASC.aspx)
[https://www.mdcalc.com/has-bled-score-major-bleeding-risk](https://www.mdcalc.com/has-bled-score-major-bleeding-risk)  
 [https://clincalc.com/Cardiology/Anticoagulation/HASBLED.aspx](https://clincalc.com/Cardiology/Anticoagulation/HASBLED.aspx)

**Management of hypertension**
Full NICE guidelines: [ https://www.nice.org.uk/guidance/ng136](https://www.nice.org.uk/guidance/ng136)
NICE guidelines summarised: [ https://cks.nice.org.uk/topics/hypertension-not-diabetic/](https://cks.nice.org.uk/topics/hypertension-not-diabetic/)
Geeky medics summary: [ https://geekymedics.com/hypertension/](https://geekymedics.com/hypertension/)

**Examples of other decision support tools (in cancer)**
[http://www.cancerresearchuk.org/health-professional/diagnosis/suspected-cancer-referral-best-practice/clinical-decision-support-tools-overview](http://www.cancerresearchuk.org/health-professional/diagnosis/suspected-cancer-referral-best-practice/clinical-decision-support-tools-overview)
