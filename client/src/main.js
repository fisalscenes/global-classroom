import { createApp } from "vue";

import mitt from "mitt";

import "v-calendar/dist/style.css";
import VCalendar from "v-calendar";

import App from "./App.vue";
import router from "./router";

import Button from "@/components/Button.vue";
import Warning from "@/components/Warning.vue";
import Header from "@/components/Header.vue";
import SideNav from "@/components/SideNav.vue";

const app = createApp(App);
const emitter = mitt();

app.config.globalProperties.emitter = emitter;

app.use(router);

app.use(VCalendar, {});

// Global components
app.component("Button", Button);
app.component("Warning", Warning);
app.component("Header", Header);
app.component("SideNav", SideNav);

app.mount("#app");
